# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 16:55:33 2012

@author: sam
"""

import sys
from PySide import QtGui
#class Example(QtGui.QMainWindow):
class Example(QtGui.QWidget):
    
    def __init__(self):
        super(Example, self).__init__()
        
#        self.initUI()
        
        
        
    def fileopen(self,message = 'Open something....',
                 filters='All files (*);;Text files (*.txt)',
                defaultDir=''):
                    
        fnames, _ = QtGui.QFileDialog.getOpenFileNames(self, message,
                        defaultDir, filter=filters)
                        
        return fnames
#        if dialog.ShowModal() == wxID_OK:
#          # We'll have to make room for multiple files here
#          selected = dialog.GetPaths()
#            #for selection in selected:
#                # print 'Selected:', selection
#        else:
#           #print 'Nothing was selected.'
#           selected=[]
#        return selected
#        dialog.Destroy()
#
#def filesave(name='untitled.txt'):
#    application = wxPySimpleApp()
## Create a save file dialog
#    dialog = wxFileDialog ( None, style = wxSAVE | wxOVERWRITE_PROMPT )
## Show the dialog and get user input
#   # if dialog.ShowModal() == wxID_OK:
#       # print 'Selected:', dialog.GetPath()
## The user did not select anything
#   # else:
#   #print 'Nothing was selected.'
## Destroy the dialog
#    #dialog.Destroy()
        
def fileopen(message = 'Open something....',
                 filters = 'All files (*.*)*.*|Text files (*.txt)|*.txt',
                defaultDir=''):
    app=QtGui.QApplication.instance()
    if not app:
        app = QtGui.QApplication(sys.argv)
    obj = Example()
    files = obj.fileopen(message = message,
                 filters = filters,
                defaultDir=defaultDir)
#    sys.exit(app.exec_())
#    app.close()
#    app.aboutToQuit.connect(app.deleteLater)
#    import pdb; pdb.set_trace()
    return files

if __name__ == "__main__":
    testDir = u'/media/Storage/Documents/00-VtResearch/Energy based/Soil Profiles'
#    app = QtGui.QApplication(sys.argv)
#    obj = Example()
    
    files = fileopen(message = 'Open something....',
                 filters= 'HDF5 Files (*.h5)',
                defaultDir=testDir)
                
    files2 = fileopen(message = 'Open something....',
                 filters= 'HDF5 Files (*.h5)',
                defaultDir=testDir)
    import pdb; pdb.set_trace()
