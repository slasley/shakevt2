# README #

ShakeVT2 is an equivalent-linear site response code written in python. It is an alternative to other codes like Shake91, Strata,  ProSHAKE, and DEEPSOIL. Because it is written in python, it is much easier to extend than the others. It was developed specifically for research at Virginia Tech. See the license for terms of use.

# Getting Started #

Start with EquivLin.py.  The functions run() and run2() should get  you started.  Also, take a look at the `__doc__` strings.

TreasIsNew.csv contains a template for inputing a soil profile.  Once you have the profile you want in a .csv file, you can input it into the program using the input_soil_profile function.  This will create a SoilProfile object which contains all details about that profile. (Alternately, you can directly call the SoilProfile object and pass arrays by keyword arguments).

A ground motion object is created by calling the GroundMotion object:

~~~~
motion = GroundMotion(filename='NGA_12_PEL090.AT2')
~~~~

The equivalent linear analysis is performed by supplying these two objects to the Dynamics object:

~~~~
result = Dynamics(soilprof, motion)
~~~~

There are a lot of keyword argument options here, so take a look at the `__doc__` strings.  Once you have your Dynamics object, you can compute other things. For example, to get a transfer function:

~~~~
tf = result.TF(1,12)
~~~~

where 1 and 12 are the layer numbers.

If you have any questions, comments, or other suggestions, contact me at slasley@vt.edu.


Required:

Python 2.7  (Python 3.* should also work, but minor bugs might be present.)

Numpy
 
Scipy

matplotlib


Suggested:

pandas

PySide


I intend to make a GUI.  I will probably use PyQt4, but we will see...

See the LICENSE.