# -*- coding: utf-8 -*-


'''
    Copyright (C) 2014 Samuel J. Lasley, Russell A. Green, and
						Adrian Rodriguez-Marek

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without restriction, 
    including without limitation the rights to use, copy, modify, merge, 
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
    BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
    ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
    SOFTWARE.
'''
import os
import sys
if os.name == 'posix':
    sys.path.append('/media/Storage/Documents/Python/Modules') # Change this path as needed; This is where I have stored the EquivLin.py and the wxfiledialog scripts/modules.
else:
    sys.path.append('E:\\Documents\\Python\\Modules')
import pdb
import numpy as np
import matplotlib.pyplot as plt
import EquivLin as el
import pandas as pd


class SHAKE(object):
    '''
    This is the overrriding object.
    '''
    def __init__(self,equivlinobj, **kwargs):
        exe_name = kwargs.get('exe_name','SHAKEVT1.EXE')
#        motformat = kwargs.get('motformat','5e15.6')
#        pdb.set_trace()
        keywords = kwargs
        run = kwargs.get('run','all')
        
        if run == 'all':
            self.inobj = SHAKEVT2to1(equivlinobj,keywords=keywords)#exe_name=exe,motformat=motformat)
            
            self.inobj.run(exe_name=exe_name)
            self.outobj = fromSHAKE(self.inobj.datname,exe_name=exe_name)
        elif run == 'readonly':
            self.outobj = fromSHAKE(kwargs.get('datname'),keywords=keywords)
        
    def plot(**kwargs):
        '''
        Plots the data from the results.
        '''
        pass
        
    
class SHAKEVT2to1(object):
    '''
    This object handles the calculation using SHAKEVT from the python EquivLin.py
    '''
#    def __init__(self,obj,**kwargs):#**kwargs):
#        pdb.set_trace()
#        if kwargs.has_key('keywords'):
#            kwargs = kwargs.get('keywords')
#        exe_name = kwargs.get('exe_name','SHAKEVT1.EXE')
#        self.motformat = kwargs.get('motformat','5e15.6')
#        if obj == None:
#            pass
#        else:
#            self.inputmaker(obj,exe_name=exe_name)
        
    
    
    def __init__(self,DynObj,**kwargs):
        '''
         Creates an input file for SHAKEVT from the profile and motion given
         as part of the Dynamics object.
        '''
        if kwargs.has_key('keywords'):
            kwargs = kwargs.get('keywords')
        exe_name = kwargs.get('exe_name','SHAKEVT1.EXE')
        self.motformat = kwargs.get('motformat','5e15.6')
        nolayers = len(DynObj.SoilObj.Vs)
        defaultname = '%s_%s_%s.dat'%(exe_name[:-4],
                                    DynObj.SoilObj.profile_name[:7],
                                    DynObj.MotObj.filename[-10:-4])
        
        name = kwargs.get('name',defaultname)
        opt6 = kwargs.get('opt6',False)
        opt8 = kwargs.get('opt8',True)
        if exe_name.lower() == 'shake91.exe':
            opt8 = False
        opt7 = kwargs.get('opt7',True)
        opt9 = kwargs.get('opt9',True)
        opt10 = kwargs.get('opt10',False) #I can't get an output from SHAKEVT for amp spectrum
        opt11 = kwargs.get('opt11',True)
              
       
        if nolayers > 13:
            print('Error:  Shake can only handle 13 layers;'+
               'your profile has {:d} layers'.format(nolayers))
        ending = '\r\n'
        f = open(name,'w')
        self.datname = name
       
        # Option 1 - Dynamic Soil Properties
        f.write(
          'Option 1 - Dynamic Soil Properties - (max is thirteen):{:s}'.format(
           ending))
    
        lines1 = []
        lines1.append('1')
        lines1.append('{:5.0f}'.format(len(DynObj.SoilObj.Vs)))
       
        G = DynObj.Gratio
        D = DynObj.damping
        gam = DynObj.gam
        nolines = len(gam)/8 
        if len(gam) % 8 > 0:
            nolines +=1
        layerlist = []
        layerlist.append('{:5.0f}'.format(nolayers))
        for i in xrange(nolayers-1):
            layerlist.append('{:5.0f}'.format(i+1))
            gamll = []
            Dll = []
            Gll = []
            for j,g in enumerate(gam):
                gamll.append(' {:9f}'.format(g*100.))
                Dll.append(' {:9f}'.format(D[i,j]*100.))
                Gll.append(' {:9f}'.format(G[i,j]))
               
            lines1.append('{:5.0f} Modulus for Layer #{:d} ({:s})'.format(
                len(gam),i+1,DynObj.DegradCurves))   
            for k in xrange(nolines):
                lines1.append(''.join(gamll[8*k:8*(k+1)]))
            for k in xrange(nolines):
                lines1.append(''.join(Gll[8*k:8*(k+1)]))
            lines1.append('{:5.0f} Damping for Layer #{:d} ({:s})'.format(
                len(gam),i+1,DynObj.DegradCurves))   
            for k in xrange(nolines):
                lines1.append(''.join(gamll[8*k:8*(k+1)]))
            for k in xrange(nolines):
                lines1.append(''.join(Dll[8*k:8*(k+1)]))
                
        # For the base layer:
        Dll = []
        Gll = []
        for j,g in enumerate(gam):
            Dll.append(' {:9f}'.format(DynObj.D[-1]*100.))
            Gll.append(' {:9f}'.format(1.))
        lines1.append('{:5.0f} Modulus for Base Layer #{:d} ({:s})'.format(
            len(gam),i+2,DynObj.DegradCurves))   
        for k in xrange(nolines):
            lines1.append(''.join(gamll[8*k:8*(k+1)]))
        for k in xrange(nolines):
            lines1.append(''.join(Gll[8*k:8*(k+1)]))
        lines1.append('{:5.0f} Damping for Base Layer #{:d} ({:s})'.format(
            len(gam),i+2,DynObj.DegradCurves))   
        for k in xrange(nolines):
            lines1.append(''.join(gamll[8*k:8*(k+1)]))
        for k in xrange(nolines):
            lines1.append(''.join(Dll[8*k:8*(k+1)]))
    
        layerlist.append('{:5.0f}'.format(i+2))
        lines1.append(''.join(layerlist))
        f.write(ending.join(lines1))  
        f.write(ending)     
    
    
        # option 2 - Soil Profile
        # Get the layer of the gwt:
        gwt = DynObj.SoilObj.gwt
        tsum = np.zeros([nolayers,],dtype=float)
        t = DynObj.SoilObj.t
        ko = DynObj.SoilObj.ko
        Vs = DynObj.SoilObj.Vs * 3.28084  # Convert from m/s to ft/s
        unit_wt = DynObj.SoilObj.unit_wt * 62.4 / (9.81 *1000.)  # Convert to ksf
        tsum[1:] = np.cumsum(t)
        t = t * 3.28084  # Convert from Meters to Feet
        for i,depth in enumerate(tsum):
            if gwt == tsum[i]:
                gwtlayer = i+1
                break
        
        lines1 = []
        lines1.append('Option 2 -- Soil Profile')
        lines1.append('2')
        lines1.append('{:5}{:5.0f}{:5.0f}  {}'.format('',
                      nolayers,
                      gwtlayer,
                      DynObj.SoilObj.profile_name)) #This number is arbitrary
    
        
        for i in xrange(nolayers-1):
            layerline1 = ('{:5.0f}{:5.0f}{:5}{:^10f}{:^10}{:^10f}' +
                        '{:^10f}{:^10.4f}{:^10.4f}')
            layerline = layerline1.format(
                        i+1,
                        i+1,
                        '',
                        t[i],  # Thickness of layer in ft
                        '',  # Max Shear Modulus in ksf  
                        0.05,  # Initial damping, <================== Change if needed
                        unit_wt[i],  # Unit Weight in ksf
                        Vs[i],  # Max Shear Wave Velocity in ft/sec
                        ko[i])
                        
            lines1.append(layerline)
        
        # For the base layer:
        if len(ko) >= i+1:
            kobase = 0.5
        else:
            kobase = ko[i+1]
        layerline1 = ('{:5.0f}{:5.0f}{:^15}{:^10}{:^10f}' +
                        '{:^10f}{:^10.4f}{:^10.4f}')
        layerline = layerline1.format(
                        nolayers,
                        nolayers,
                        '',
                        '',  # Max Shear Modulus in ksf  
                        0.01,  # Initial damping, <================== Change if needed
                        unit_wt[i+1],  # Unit Weight in ksf
                        Vs[i+1],  # Max Shear Wave Velocity in ft/sec
                        kobase)  
        lines1.append(layerline)
        f.write(ending.join(lines1))  
        f.write(ending)
        
        
        # Option 3 -- Input (Object) Motion
        NPTS = DynObj.MotObj.NPTS
        NFFT = DynObj.MotObj.NFFT
        dt = DynObj.MotObj.dt
        cutoff = kwargs.get('cutoff',1./(2.*dt))
        motfile = os.path.basename(DynObj.MotObj.filename)
        scalefactor = DynObj.MotObj.scalefactor
        
        lines1 = []
        lines1.append('Option 3 -- Input (Object) Motion:')
        lines1.append('3')
        lineA = '{:>5d}{:>5d}{:>10f}{:^30}{:10}'.format(
                NPTS,
                NFFT,
                dt,
                motfile,
                '({})'.format(self.motformat)) #<======== May need to be changed in some cases!!!
        lineB = '{:10f}{:10}{:10f}{:5d}{:5d}'.format(
                scalefactor,
                '',
                cutoff,
                4,
                int(self.motformat[0]))
        lines1.append(lineA)
        lines1.append(lineB)
        f.write(ending.join(lines1))  
        f.write(ending)
        
        
        # Option 4 - Assignment of Object Motion to a Specific Sublayer
        lines1 = []
        lines1.append('Option 4 - Assignment of Object Motion to a Specific Sublayer:')
        lines1.append('4')
        loc_mot = DynObj.SoilObj.loc_mot
        if loc_mot == 0:
            loc_mot = nolayers
        outcrop_mot = DynObj.SoilObj.outcrop_mot
        lines1.append('{:5d}{:5d}'.format(loc_mot,outcrop_mot))
        f.write(ending.join(lines1))  
        f.write(ending)
        
        
        # Option 5 -- number of iterations & ratio of avg strain to max strain
        lines1 = []
        lines1.append('Option 5 -- Number of Iterations &' +
                        ' Ratio of Avg Strain to Max Strain')
        lines1.append('5')
        strainratio = DynObj.strainratio
        iters = DynObj.iters
        lines1.append('{:5d}{:5d} {:9f}'.format(
                        0, # 1 to save strain-compatible soil properties, 0 other
                        iters,
                        strainratio))
        f.write(ending.join(lines1))  
        f.write(ending)
        
        
        #Option 6 -- Computation of Acceleration at Top of Specified Sublayers
        # This first section is to get the max acceleration profile
        
        lines1 = []
        lines1.append('Option 6 -- Computation of Acceleration at ' +
                        'Top of Specified Sublayers')
        lines1.append('6')
        lineA = ''
        lineB = ''
        lineC = ''
        if opt6 == True:
            opt6layers = kwargs.get('opt6layers',[1,11])
            opt6outcrop = kwargs.get('opt6outcrop',[0])
            if len(opt6layers) != len(opt6outcrop):
                opt6outcrop = [opt6outcrop[0]] * len(opt6layers)
            
            for i in xrange(nolayers):
                lineA += '{:5}'.format(i+1)
                if i+1 in opt6layers:
                    lineB += '{:5}'.format(opt6outcrop[opt6layers.index(i+1)])
                    lineC += '{:5}'.format(1)
                else:
                    
                    lineB += '{:5}'.format(1)  #<=== 1 for within, 0 for outcrop
                    lineC += '{:5}'.format(0)  #<=== 0 for max only, 1 for max and timehistory   
        else: 
            for i in xrange(nolayers):
                lineA += '{:5}'.format(i+1)
                lineB += '{:5}'.format(1)  #<=== 1 for within, 0 for outcrop
                lineC += '{:5}'.format(0)  #<=== 0 for max only, 1 for max and timehistory
            
        lines1.append(lineA)
        lines1.append(lineB)
        lines1.append(lineC)
        f.write(ending.join(lines1))  
        f.write(ending)
        
        # This section is to get the 
        
        
        # option 8 -- compute number of shear stress cycles by equating energy:
        if opt8 == True:
            lines1 = []
            lines1.append('Option 8 -- Compute Number of Shear Stress ' + 
                            'Cycles by Equating Energy:')
            lines1.append('8')
            f.write(ending.join(lines1))  
            f.write(ending)
        
        # Option 7 -- shear stress and strain timehistory outputs
        if opt7 == True:
            opt7layers = kwargs.get('opt7layers',[2,5])
            if opt7layers == 'all':
                opt7layers = np.arange(2,len(DynObj.SoilObj.t)+1) # Start at 2; no stress or strain at the surface
            lines1 = []
            count = 0
            for i, value in enumerate(opt7layers):
#                count += 1
#                if count == 1:
                lines1.append('Option 7 -- Shear Stress/Strain Time Histories')
                lines1.append('7')
#                    lines2.append('Option 7 -- Shear Strain Time Histories')
#                    lines2.append('7')
#                else:
#                    count = 0
                lines1.append(
                    '{:>5d}{:>5d}{:>5d}{:>5}{:>5d}{:10}{}'.format(
                    value, # number of sublayer 1-5
                    1,  # set equal to 0 for strain or 1 for stress 6-10
                    1,  # set equal to 1 to save time history
                    '',  # leave blank 16-20
                    NPTS,  # number of values to be saved, typ. NPTS 21-25
                    '',  # leave blank 26-35
                    ''))#'-- stress in layer {}'.format(value))) # id info 36-65
                lines1.append(
                    '{:>5d}{:>5d}{:>5d}{:>5}{:>5d}{:10}{}'.format(
                    value, # number of sublayer 1-5
                    0,  # set equal to 0 for strain or 1 for stress 6-10
                    1,  # set equal to 1 to save time history
                    '',  # leave blank 16-20
                    NPTS,  # number of values to be saved, typ. NPTS 21-25
                    '',  # leave blank 26-35
                    ''))#'-- strain in layer {}'.format(value))) # id info 36-65
                    
            f.write(ending.join(lines1))  
            f.write(ending)
#            f.write(ending.join(lines2))  
#            f.write(ending)
            
        # Option 9 -- Response Spectrum
        if opt9 == True:
            opt9layers = kwargs.get('opt9layers',[1,6])
            opt9outcrop = kwargs.get('opt9outcrop',[0]) # 0 for outcropping, 1 for within
            opt9dampingratios = kwargs.get('opt9dampingratios',[0.05])
            opt9gravity = kwargs.get('opt9gravity',1.0)
            if len(opt9layers) != len(opt9outcrop):
                opt9outcrop = [opt9outcrop[0]] * len(opt9layers)
            if len(opt9layers) != len(opt9dampingratios):
                opt9dampingratios = [opt9dampingratios[0]] * len(opt9layers)
            for i,value in enumerate(opt9layers):
              opt9line3 = ''
              try: 
                  dampinglen = len(opt9dampingratios[i])
                  for j in xrange(dampinglen):
                      opt9line3 += ' {}'.format(opt9dampingratios[i][j])
              except TypeError: 
                  dampinglen = 1
                  opt9line3 += ' {}'.format(opt9dampingratios[i])
              lines1 = []
              lines1.append('Option 9 -- Response Spectrum Layer {}'.format(
                                                      value))
              lines1.append('{:d}'.format(9))
              lines1.append('{:>5d}{:>5d}'.format(value,opt9outcrop[i]))
              lines1.append('{:>5d}{:>5d}{:>10.3f}'.format(
                  dampinglen,
                  0,
                  opt9gravity))
              lines1.append(opt9line3)
              f.write(ending.join(lines1))  
              f.write(ending)
            
        # Option 10 -- Amplification Spectrum
        if opt10 == True:
            opt10layers = kwargs.get('opt10layers',[[nolayers,1]])
            opt10outcrop = kwargs.get('opt10outcrop',[[0,0]])
            opt10dfreq = kwargs.get('opt10dfreq',[0.125])
            if len(opt10dfreq) != len(opt10layers):
                opt10dfreq = [opt10dfreq[0]] * len(opt10layers)
            if len(opt10outcrop) != len(opt10layers):
                opt10outcrop = [opt10outcrop[0]] * len(opt10layers)
                
            for i,value in enumerate(opt10layers):
                if opt10outcrop[i][0] == 0:
                    mot1 = 'outcrop'
                else:
                    mot1 = 'within'
                if opt10outcrop[i][1] == 0:
                    mot2 = 'outcrop'
                else:
                    mot2 = 'within'
                lines1 = []
                lines1.append('Option 10 -- Amplification Spectrum')
                lines1.append('{:<d}'.format(10))
                lines1.append('{:>5d}{:>5d}{:>5d}{:>5d}{:>10.4f}{:^48}'.format(
                            value[0],  # number of 1st sublayer 1-5
                            opt10outcrop[i][0],  # 0 for outcropping, 1 for within 6-10
                            value[1],  # number of 2nd sublayer 11-15
                            opt10outcrop[i][1], # 0 for outcropping, 1 for within 16-20
                            opt10dfreq[i],  # freq step in cyc/sec  21-30
                            ' {} mot of {} / {} mot of {}'.format(
                                    mot2,value[1],mot1,value[0]))) # id info 31-78
                            
                f.write(ending.join(lines1))  
                f.write(ending)
            
            
        # Option 11 -- Fourier Spectrum
        if opt11 == True:
            opt11layers = kwargs.get('opt11layers',[1,4]) # I get an error when I try to run more than 2 layers thru SHAKE.
            opt11outcrop = kwargs.get('opt11outcrop',[0])
            opt11nosmooth = kwargs.get('opt11nosmooth',0)
            opt11nfft = kwargs.get('opt11nfft',NFFT/2+1)
            if len(opt11outcrop) != len(opt11layers):
                opt11outcrop = [opt11outcrop[0]] * len(opt11layers)
            count = 0
            lines1 = []
            for i, value in enumerate(opt11layers):
                count += 1
                if count == 1:
                    lines1.append('Option 11 -- Fourier Spectrum')
                    lines1.append('{:<d}'.format(11))
                else:
                    count = 0
                lines1.append('{:>5d}{:>5d}{:>5d}{:>5d}{:>5d}'.format(
                            value,  # number of sublayer 1-5
                            opt11outcrop[i],  # 0 for outcropping, 1 for within 6-10
                            2,  # 2 if spectrum is to be saved to file 11-15
                            opt11nosmooth,  # num of times spectrum is smoothed 16-20
                            opt11nfft))  # number of values to save
            if count == 1:  # Repeat so that there are even numbers of requests 
                lines1.append('{:>5d}{:>5d}{:>5d}{:>5d}{:>5d}'.format(
                            value,  # number of sublayer 1-5
                            opt11outcrop[i],  # 0 for outcropping, 1 for within 6-10
                            2,  # 2 if spectrum is to be saved to file 11-15
                            opt11nosmooth,  # num of times spectrum is smoothed 16-20
                            opt11nfft))  # number of values to save
            f.write(ending.join(lines1))  
            f.write(ending)
        
        # Program termination
        lines1 = []
        lines1.append('execution will stop when program encounters 0')
        lines1.append('0')
        f.write(ending.join(lines1))  
        f.write(ending)
        f.close()
#        return name
#        pdb.set_trace()
        
    def run(self,**kwargs):
        '''
        Calls the SHAKEVT executable.
        '''
        exe_name = kwargs.get('exe_name','SHAKEVT1.EXE')
        basename = os.path.basename(self.datname)
        bbname = os.path.splitext(basename)[0]
        out1 = bbname + '.ot1'
        out2 = bbname + '.ot2'
        self.out1 = out1
        self.out2 = out2        
        
        if os.path.isfile(out1):
            print('Erasing {}!!!'.format(out1))
            os.remove(out1)
    
        if os.path.isfile(out2):
            print('Erasing {}!!!'.format(out2))
            os.remove(out2)
    
        f = open('tempout.txt','w')
        f.write('{}\r\n{}\r\n{}\r\n'.format(basename,out1,out2))
        f.close()
        if os.name == 'posix':
            os.system('wine {} < tempout.txt'.format(exe_name))
        else:
            os.system('{} < tempout.txt'.format(exe_name))
        os.remove('tempout.txt')
        
        
class fromSHAKE(object):
    '''
    Retrieves data from the SHAKE output files.
    '''
    def __init__(self,datname,**kwargs):
        
        self.datname = datname
        exe_name = kwargs.get('exe_name','SHAKEVT1.EXE')
        
        basename = os.path.basename(datname)
        bbname = os.path.splitext(basename)[0]
        out1 = bbname + '.ot1'
        out2 = bbname + '.ot2'
        self.ot1 = open(out1, 'r')
        self.ot2 = open(out2, 'r')
        self._ot1parser(exe_name=exe_name)
        self._ot2parser(exe_name=exe_name)
        self.ot1.close()
        self.ot2.close()
        
#        pdb.set_trace()
        
        
    def _ot1parser(self,**kwargs):
        '''
        Parses ot1 file.
        '''
        exe_name = kwargs.get('exe_name','SHAKEVT1.EXE')
        for line in self.ot1:
#            print line
            split = line.split()
            if '  TIME STEP FOR INPUT MOTION =' == line[:30]:
                self.dt = float(split[-1])
            if 'OPTION' in split:
                
                if '6' in split:
                    self._opt6parser()
                if '9' in split:
                    self._opt9parser()
                if '11' in split:
                    self._opt11parser(exe_name=exe_name)
#        pdb.set_trace()
        # Get profile info
        
        
                                    
    def _ot2parser(self,**kwargs):
        
        read = True
        line = self.ot2.readline()
        while read:
            if not line:
                read = False
                break
            
            if 'NORMALIZED' in line.split():
                line = self.summary(line)
                continue
            elif 'STRESS' in line:
                line = self.stress(line)
                continue
            elif 'STRAIN' in line:
                line = self.strain(line)
                continue
            elif 'ACCELERATION' in line:
                line = self.accel(line)
                continue
            else:
                line = self.ot2.readline()
            
    
            
                
    def summary(self,line):
        count = 0
        sumlines = []
        while True:
            count += 1
            line = self.ot2.readline()
            if count >= 3:
                try:
                    float(line.split()[0])
                except ValueError:
                    names = ['Layer','Depth','alpha','MaxGam',
                             'MaxTau','G','D','N','DE']
                    self.profile2 = pd.DataFrame(sumlines,columns=names)
                    break
                else:
                    try:
                        sumlines.append([int(val) if i == 0 else  
                        float(val)  for i,val in enumerate(line.split())])
                    except ValueError:
                        for i,val in enumerate(line.split()):
                            if i == 0:
                                try:
                                    sumlines.append(int(val))
                                except ValueError:
                                    sumlines.append(-111)
                            else:
                                try:
                                    sumlines.append(float(val))
                                except ValueError:
                                    sumlines.append(float(-1111))
        return line                                       

                    
    def stress(self,line):
        widcols = 9  #Format of stress/strain is 8 columns of width of 9
        nocols = 8
        lines = []
        layer = int(line.split()[-1])
        while True:
            line = self.ot2.readline()
            try:
#                float(line.split()[0])
                float(line[:widcols])
            except (ValueError,IndexError):
                try:
                    self.tau.append(
                        [layer,np.array(lines).ravel()])
                except (NameError, AttributeError):
                    self.tau = []
                    self.tau.append([layer,np.array(lines).ravel()])
                break
            else:
#                try:
#                    lines.append([float(val) for val in line.split()[:-1]])
#                except ValueError: # Sometimes there is no space between columns
                
                lines.append([float(line[i:i+widcols]) 
                    for i in xrange(0,widcols*nocols,widcols)])                     
        return line

                
    def strain(self,line):
        widcols = 9  #Format of stress/strain is 8 columns of width of 9
        nocols = 8
        lines = []
        layer = int(line.split()[-1])
        while True:
            line = self.ot2.readline()
            try:
#                float(line.split()[0])
                float(line[:widcols])
            except (ValueError,IndexError):
                try:
                    self.gam.append(
                        [layer,np.array(lines).ravel()])
                except (NameError, AttributeError):
                    self.gam = []
                    self.gam.append([layer,np.array(lines).ravel()])
                break
            else:
#                try:
#                    lines.append([float(val) for val in line.split()[:-1]])
#                except ValueError: # Sometimes there is no space between columns
                lines.append([float(line[i:i+widcols]) 
                    for i in xrange(0,widcols*nocols,widcols)])   
        return line
        
        
    def accel(self,line):
        widcols = 9  #Format of stress/strain is 8 columns of width of 9
        nocols = 8
        lines = []
        layer = int(line.split()[-3])
        while True:
            line = self.ot2.readline()
            try:
#                float(line.split()[0])
                float(line[:widcols])
            except ValueError:
                try:
                    self.acc.append(
                        [layer,np.array(lines).ravel()])
                except (NameError, AttributeError):
                    self.acc = []
                    self.acc.append([layer,np.array(lines).ravel()])
                break
            else:
#                try:
#                    lines.append([float(val) for val in line.split()[:-1]])
#                except ValueError: # Sometimes there is no space between columns
                
                lines.append([float(line[i:i+widcols]) 
                    for i in xrange(0,widcols*nocols,widcols)])
        return line

                
    def _opt6parser(self):
        count = 0
        lines = []
        for line in self.ot1:
#            print(line)
#            pdb.set_trace()
            count += 1
           
            if count >= 6:
                try:
                    float(line.split()[3])
                except (ValueError,IndexError):
                    break
                else:
                    lines.append([val if i == 0 else float(val) 
                                for i,val in enumerate(line.split())])
                        
        names = ['mottype','depth','MaxAcc','Time','MeanSqFr','AccRatio',
                 'TH saved']
#        pdb.set_trace()
        self.profile1 = pd.DataFrame(lines,columns=names)
        self.nolayers = self.profile1.shape[0]
        
    def _opt9parser(self):
        '''
        Response Spectra
        '''
        count = 0
        T = []
        RelD = []
        RelV = []
        PsRelVel = []
        AbsAcc = []
        PsAbsAcc = []
        Freq = [] 
        sec1 = False
        sec2 = False
        sec2lines = []
        for line in self.ot1:
            count += 1
#            print(line)
            if count == 1:
                layer = int(line.split()[-1])
            if count == 2:
                damping = float(line.split()[-1])
            if count >= 9:
                if sec1 == True:
                    try:
                        float(line.split()[2])
                    except:
                        sec1 = False
                        break
                    else:
                       T.append(float(line.split()[1]))
                       RelD.append(float(line.split()[2]))
                       RelV.append(float(line.split()[3]))
                       PsRelVel.append(float(line.split()[4]))
                       AbsAcc.append(float(line.split()[5]))
                       PsAbsAcc.append(float(line.split()[6]))
                       Freq.append(float(line.split()[7]))
                
                if  'NO.' in line.split():
                    sec1 =  True
                    continue
                if sec2 == True:
                    try:
                        float(line.split()[0])
                    except:
                        break
                    else:
                        sec2lines.append([float(i) for i in line.split()])
                
#        sec2lines = np.array(sec2lines)
        T,RelD,RelV,PsRelVel,AbsAcc,PsAbsAcc,Freq = [np.array(arr) for arr in 
                                [T,RelD,RelV,PsRelVel,AbsAcc,PsAbsAcc,Freq]]
        try:
            self.RespSpec.append([layer,damping,T,RelD,RelV,PsRelVel,
                                  AbsAcc,PsAbsAcc,Freq])
        except (NameError, AttributeError):
            self.RespSpec = []
            self.RespSpec.append([layer,damping,T,RelD,RelV,PsRelVel,
                                  AbsAcc,PsAbsAcc,Freq])

        
    def _opt11parser(self,**kwargs):
        exe_name = kwargs.get('exe_name','SHAKEVT1.EXE')
        count = 0
        lines = []
        layers = []
        freq = []
        for line in self.ot1:
            count += 1
            if count == 1:
                layers.append(int(line.split()[2]))
                try:
                    line.split()[3]
                    nexcount = 3
                    datacount = 5
                except IndexError:
                    nexcount = 2
                    datacount = 4
            if count == nexcount:
                layers.append(int(line.split()[2]))
            if 'FREQ' in line.split():
                datacount = count + 1
            if count >= datacount:
                try:
                    float(line.split()[2])
                    lines.append([float(i) for i in line.split()[1:]])
                    freq.append(line.split()[0])
                except (ValueError,IndexError):
                    break
        FAS = np.array(lines)
        Nyquist =1./(2.*self.dt)
#            self.df = Nyquist/len(FAS)
        try:
            float(freq[-1])
            freq = np.array(freq,dtype=float)
        except ValueError:
            freq = np.linspace(0,Nyquist,num=len(FAS),endpoint=True)
        
#        pdb.set_trace()
        for i in xrange(FAS.shape[1]):
            try:
                self.FAS.append([layers[i],freq,FAS[:,i]])
            except (NameError, AttributeError):
                self.FAS = []
                self.FAS.append([layers[i],freq,FAS[:,i]])
            
#            try:
#                self.FASfreq.append(freq)
#            except (NameError, AttributeError):
#                self.FASfreq = []
#                self.FASfreq.append(freq)
                
#            plt.loglog(freq,FAS[:,i])
#        pdb.set_trace()
#        try:
#            FAS.append(np.array())
            
            
    
def go(profile='../Modules/ProfileBmet.csv', 
        motion_name='NGA_no_31_C08050_6e15.6.AT2',
        Degrad='DS', iters=20, error=0.01, plotting=False):

    sprofile = el.inputread(profile)

    gmotion = el.GroundMotion(filename=motion_name,Butrwrth=False)
#    pdb.set_trace()
    result = el.Dynamics(sprofile,gmotion,
                         Error=error,
                         modtype='FreqInd',
                         DegradCurves=Degrad,
                        verbose=False)
#    SHAKE = SHAKEVT2to1()                  
#    SHAKE.inputmaker(result)
#    SHAKE1 = SHAKEVT2to1(result)
#    SHAKE.run()
    shake = SHAKE(result, opt6=True, opt6layers=[1,10], opt6outcrop=[1,0],
                  opt10=True,motformat='6e15.6',exe_name='Shake91.exe')
    pdb.set_trace()
    

def take():
    k = fromSHAKE('Testing_YBI090.dat')
    pdb.set_trace()
    
#    Testing_YBI090.ot2


if __name__ == '__main__':
#    go()

    take()