#!/usr/bin/python
# IZ_1993.py

'''
    Copyright (C) 2014 Samuel J. Lasley, Russell A. Green, and
						Adrian Rodriguez-Marek

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without restriction, 
    including without limitation the rights to use, copy, modify, merge, 
    publish, distribute, sublicense, and/or sell copies of the Software, 
    and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
    BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
    ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
    SOFTWARE.
'''

#import pdb
import numpy as np
#straindef=np.logspace(-6,-0.004364805,num=10)
straindef = np.array([0.000001, 0.000003, 0.00001, 0.00003, 0.0001, 0.0003,
                      0.001, 0.003, 0.01, 0.99, 5])


def IZ_1993(gam=straindef, PI=0., sigm=100):
    ''' Returns the Ishibashi and Zhang (1993) values for G/Gmax
    and damping (decimal) for a given values of strain (as a decimal),
    Plasticity index, and initial mean effective confining stress (kPa).
    '''
    PI = float(PI)
    sigm = float(sigm)

    if PI == 0.:
        n = 0.0
    elif PI <= 15:
        n = 3.37e-6 * PI ** 1.404
    elif PI <= 70:
        n = 7e-7 * PI ** 1.976
    else:
        n = 2.7e-5 * PI ** 1.115


    K = 0.5 * (1 + np.tanh(np.log(((0.000102 + n) / gam) ** 0.492)))
    m = (0.272 * (1 - np.tanh(np.log((0.000556 / gam) ** 0.4))) *
                np.exp(-0.0145 * PI ** 1.3))
    Gratio = K * sigm ** m
    np.putmask(Gratio, Gratio>1, 1)
    #use np.copyto when updates to numpy 1.7 or use Gratio.clip(max=1)
    D = 0.1665 * (1 + np.exp(-0.0145 * PI ** 1.3)) * (0.586 * Gratio ** 2. - 
                1.547 * Gratio + 1.)

    return (gam, Gratio, D)


def DS_2001(gam=straindef, PI=0., sigm=100., OCR=1., soil=0, N=15., frq=1.):
    ''' 
    Darendeli and Stokoe (2001) shear modulus reduction and damping curves.
    gam= shear strain (decimal)
    PI= Plasticity Index
    sigm = the initial mean effective confining stress (kPa)
    OCR = the overconsolidation ratio
    soil = the soil type indicator:
        0 for general
        1 for clean sand
        2 for sands with high fines contents
        3 for silts
        4 for clays
    N = the number of cycles
    frq = the frequency of loading
    Dmintype = 1 for 'Default', 0 for 'Green'  (Suggest using Green's for now!!!!!)
                !!!Not Implemented !!!
    The output of the function is:
    gam = the same input shear strain as a decimal
    Gratio = G/Gmax = the shear modulus normalized by the small strain
        shear modulus.
    D = damping (decimal)
    '''
    PI = float(PI)
    sigm = float(sigm)
    OCR = float(OCR)
    N = float(N)
    frq = float(frq)
    soil = int(soil)
    phi1 = np.array([ 3.52E-02, 4.74E-02, 3.34E-02, 4.16E-02, 2.58E-02])
    phi2 = np.array([ 1.01E-03,-2.34E-03,-5.79E-05, 6.89E-04, 1.95E-03])
    phi3 = np.array([ 3.246E-01, 2.50E-01, 2.49E-01, 3.21E-01, 9.92E-02])
    phi4 = np.array([ 3.483E-01, 2.34E-01, 4.82E-01, 2.80E-01, 2.26E-01])
    phi5 = np.array([ 9.19E-01, 8.95E-01, 8.45E-01, 1.00E+00, 9.75E-01])
    phi6 = np.array([ 8.005E-01, 6.88E-01, 8.89E-01, 7.12E-01, 9.58E-01])
    phi7 = np.array([ 1.29E-02, 1.22E-02, 2.02E-02, 3.03E-03, 5.65E-03])
    phi8 = np.array([-1.069E-01, -1.00E-01,-1.00E-01,-1.00E-01,-1.00E-01])
    phi9 = np.array([-2.889E-01, -1.27E-01,-3.72E-01,-1.89E-01,-1.96E-01])
    phi10 = np.array([ 2.919E-01, 2.88E-01, 2.33E-01, 2.34E-01, 3.68E-01])
    phi11 = np.array([ 6.329E-01, 7.67E-01, 7.76E-01, 5.92E-01, 4.66E-01])
    phi12 = np.array([-5.66E-03, -2.83E-02,-2.94E-02,-7.67E-04, 2.23E-02])

    # Calculate the reference shear strain:
    gamr = ((phi1[soil] + phi2[soil] * PI * OCR ** phi3[soil])
             * (sigm / 101.325) ** phi4[soil])
    gam = gam * 100  # convert to percent
    
    # Calculate assorted variables:
    a = phi5[soil]
    b = phi11[soil] + phi12[soil] * np.log(N)
    C1 = -1.1143 * a ** 2 + 1.8618 * a + 0.2523
    C2 = 0.0805 * a ** 2 - 0.0710 * a - 0.0095
    C3 = -0.0005 * a ** 2 + 0.0002 * a + 0.0003
    # Calculate G/Gmax
    Gratio = 1. / (1. + (gam / gamr) ** a)

    
    Dmasinga1 = 100 / np.pi * (4 * (gam - gamr * np.log((gam + gamr) 
                / gamr)) / (gam ** 2 / (gam + gamr)) - 2 )
                
    Dmasing = C1 * Dmasinga1 + C2 * Dmasinga1 ** 2 + C3 * Dmasinga1 ** 3
    
    Dmin = ((phi6[soil] + phi7[soil] * PI * OCR ** phi8[soil])
            * (sigm / 101.3) ** phi9[soil] * (1 + phi10[soil] * np.log(frq)))
    
    D = (b * (Gratio ** 0.1) * Dmasing + Dmin) / 100. #Return damping to a decimal


    return gam / 100, Gratio, D



